﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteScrollManager : MonoBehaviour
{
	public StageController stage_controller;
	public GameObject Bg_Unit_Prefab;
	public GameObject Bg_Unit_Obj { get; private set; }
	public float bg_unit_width { get; private set; }
	private float _cam_width = -1;
	public float cam_width
	{
		get
		{
			if(_cam_width == -1)
			{
				_cam_width = calculate_cam_width();
			}
			return _cam_width;
		}
	}
	Camera stageCamera
	{
		get
		{
			return stage_controller.stageCamera;
		}
	}
	// Use this for initialization
	void Start ()
	{
		bg_unit_width = Bg_Unit_Prefab.GetComponent<prefab_bg_unit>().bg_unit_width;
		Bg_Unit_Obj = Instantiate(Bg_Unit_Prefab);
	}
	
	// Update is called once per frame
	void Update ()
	{
		//Debug.Log("Cam max x : " + stageCamera.OrthographicBounds().max.x);
		//Debug.Log("Bg Obj Max x : " + (Bg_Unit_Obj.transform.position.x + bg_unit_width / 3f));
		if(stageCamera.OrthographicBounds().max.x >= Bg_Unit_Obj.transform.position.x + bg_unit_width / 3f)
		{
			Bg_Unit_Obj.transform.position = new Vector3(Bg_Unit_Obj.transform.position.x + bg_unit_width / 3f, 0, 0);
		}
	}

	float calculate_cam_width()
	{
		float height = 2f * stageCamera.orthographicSize;
		float width = height * stageCamera.aspect;
		return width;
	}
}
