﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	[HideInInspector]
	public StageController stageController;

	public GameObject hit_enemy_particle_prefab, damagetext_prefab;

	public Vector2 startPos;

	public int attack = 10;


	public float Accel_X;
	public float MaxVelX;
	public float MaxVel_On_Tap;
	public float Velocity_Add_On_Tap;
	/// <summary>
	/// 1초에 걸쳐 Max Tap Vel에서 MaxVel로 떨어져내려감
	/// </summary>
	public float Speed_Damp_Time = 1f;
	bool tapped = false;

	public float current_vel_x { get; private set; }
	BoxCollider2D hitbox;
	// Use this for initialization
	void Start ()
	{
		hitbox = GetComponent<BoxCollider2D>();
	}

	// Update is called once per frame
	GarlicTimer tap_timer = new GarlicTimer(0.1f);
	GarlicTimer lose_speed_timer = new GarlicTimer(1f);
	void Update ()
	{
		//Debug.Log("Tap Timer State : " + tap_timer.timer_state.ToString());
		if ( tapped == false && Input.anyKeyDown)
		{
			tapped = true;
			current_vel_x += Velocity_Add_On_Tap;
			current_vel_x = Mathf.Clamp(current_vel_x, 0, MaxVel_On_Tap);
			tap_timer.Start();

			lose_speed_timer.Stop();
			lose_speed_timer.Start();
			Debug.Log("Tap! vel : " + current_vel_x);			
		}
		
		current_vel_x += Accel_X * Time.deltaTime;

		if(lose_speed_timer.Finished == false)
		{
			if (current_vel_x >= MaxVel_On_Tap)
			{
				current_vel_x = MaxVel_On_Tap;
			}
			else if (current_vel_x > MaxVelX)
			{
				current_vel_x -= 0.1f;
			}
			else if (current_vel_x <= MaxVelX)
			{
				lose_speed_timer.Stop();
			}
		}
		else
		{
			if (current_vel_x >= MaxVelX)
			{
				current_vel_x -= 0.1f;
			}
		}
		//if(tapped == false)
		//{
		//	if(current_vel_x >= MaxVelX)
		//	{
		//		current_vel_x = MaxVelX;
		//	}
		//}
		//else
		//{
		//	if (current_vel_x >= MaxVel_On_Tap)
		//	{
		//		current_vel_x = MaxVel_On_Tap;
		//	}
		//	else if(current_vel_x > MaxVelX)
		//	{
		//		current_vel_x -= 0.1f;
		//	}
		//	else if(current_vel_x <= MaxVelX)
		//	{
		//		tapped = false;
		//	}
		//	Debug.Log("tapped true, vel : " + current_vel_x);
		//}

		transform.position =
			new Vector3(
					transform.position.x + current_vel_x * Time.deltaTime,
					transform.position.y,
					transform.position.z
				);

		checkEnemyCollision();

		lose_speed_timer.count(Time.deltaTime);
		tap_timer.count(Time.deltaTime);
		if(tap_timer.Finished)
		{
			tapped = false;
			tap_timer.Stop();
		}
		//Debug.Log("Player Pos X : " + transform.position.x + ", velX : " + current_vel_x);
	}

	const float BLOCK_TAP_TIME = 0.05f;
	void checkEnemyCollision()
	{
		foreach(EnemyController cont in stageController.enemySpawnedList)
		{
			if (cont.hitbox.bounds.Intersects(hitbox.bounds))
			{
				
				float moveOutPlayerX = Mathf.Floor(cont.hitbox.bounds.min.x - hitbox.bounds.size.x / 2f - hitbox.offset.x - 1);

				transform.position = new Vector3(moveOutPlayerX,
						transform.position.y,
						transform.position.z
					);
				tap_timer.Stop();
				tap_timer.Start();

				current_vel_x = -48f;
				GameObject hit_particle = Instantiate(hit_enemy_particle_prefab);
				Vector3 attack_pos = hitbox.bounds.center;
				attack_pos.x += hitbox.bounds.size.x / 2f;
				hit_particle.transform.position = attack_pos;

				GameObject damage_text = Instantiate(damagetext_prefab);
				Vector3 damageTextSpawnAt = cont.hitbox.bounds.center;
				damageTextSpawnAt.y = cont.hitbox.bounds.max.y + 2;
				damage_text.GetComponent<DamageTextAnimation>().initialize(damageTextSpawnAt, 10);
			}
		}
	}

	public void Initialize(StageController controller)
	{
		stageController = controller;
		transform.position = new Vector3(
				startPos.x,
				startPos.y,
				transform.position.z
			);
	}
}
