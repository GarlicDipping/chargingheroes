﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarlicTimer
{
	public enum state
	{
		Stopped,
		Paused,
		Running
	}
	public state timer_state { get; private set; }
	float time_to_count = 0;
	float counter = 0;

	private bool _Ready;
	public bool Finished
	{
		get
		{
			if(_Ready == true)
			{
				_Ready = false;
				return true;
			}
			else
			{
				return false;
			}
		}
		private set { _Ready = value; }
	}
	public GarlicTimer(float time_to_count)
	{
		this.time_to_count = time_to_count;
		Finished = true;
		counter = 0;
		timer_state = state.Stopped;
		update_running(0);
	}

	public void count(float deltaTime)
	{
		if(timer_state == state.Running)
		{
			update_running(deltaTime);
		}
	}

	void update_running(float deltaTime)
	{
		counter += deltaTime;
		if (counter >= time_to_count)
		{
			Finished = true;
			counter = 0;
		}
	}

	public void Start()
	{
		timer_state = state.Running;
		Finished = false;
		counter = 0;
	}

	public void Stop()
	{
		timer_state = state.Stopped;
		Finished = true;
		counter = 0;
	}

	public void Pause()
	{
		timer_state = state.Paused;
	}

	public void Resume()
	{
		timer_state = state.Running;
	}
}
