﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour
{
	public Camera stageCamera;

	public GameObject playerPrefab;
	public GameObject playerObj { get; private set; }

	public GameObject[] enemyPrefabs;
	public int enemy_appear_gap_min = 4;
	public int enemy_appear_gap_max = 8;
	public int enemy_spawn_count_min = 1;
	public int enemy_spawn_count_max = 5;

	[HideInInspector]
	public List<EnemyController> enemySpawnedList { get; private set; }

	public Vector2 start_position = new Vector2(0, 80);
	// Use this for initialization
	void Start ()
	{
		Initialize();
	}
	
	// Update is called once per frame
	void Update ()
	{
		setCameraPos();
		checkEnemySpawn();
	}

	void setCameraPos()
	{
		float target_x = playerObj.transform.position.x;
		float x = Mathf.Clamp(target_x, start_position.x, float.MaxValue);
		setCameraPosX(x);
	}

	/// <summary>
	/// 적 스폰시 플레이어에게서 최소한 떨어져있어야하는 거리
	/// </summary>
	int min_spawn_from_player = 320;
	/// <summary>
	/// 다음 적은 플레이어에게서 어디에 스폰할까
	/// </summary>
	float next_enemy_spawn_at = 160;
	void checkEnemySpawn()
	{
		float distance_to_spawn_pos = (next_enemy_spawn_at - playerObj.transform.position.x);
		if (distance_to_spawn_pos  <= min_spawn_from_player)
		{
			spawn_enemy(next_enemy_spawn_at);
			next_enemy_spawn_at = playerObj.transform.position.x + min_spawn_from_player +
				Random.Range(enemy_appear_gap_min, enemy_appear_gap_max) * 16;
		}
	}

	void spawn_enemy(float spawnAt)
	{
		int enemy_spawn_count = Random.Range(enemy_spawn_count_min, enemy_spawn_count_max);
		for(int i = 0; i < enemy_spawn_count; i++)
		{
			int enemy_idx = Random.Range(0, enemyPrefabs.Length);
			GameObject enemy = Instantiate(enemyPrefabs[enemy_idx]);
			float dist_from_spawnpos = 1;
			enemy.transform.position = new Vector3(
					spawnAt + i * dist_from_spawnpos * 16,
					enemy.transform.position.y,
					enemy.transform.position.z
				);
			enemySpawnedList.Add(enemy.GetComponent<EnemyController>());
		}
	}

	private void Initialize()
	{
		enemySpawnedList = new List<EnemyController>();
		playerObj = Instantiate(playerPrefab);
		playerObj.GetComponent<PlayerController>().Initialize(this);
		setCameraPos(start_position);
	}

	#region Camera Position Set 

	void setCameraPosX(float x)
	{
		stageCamera.transform.position = new Vector3(
				x, stageCamera.transform.position.y, stageCamera.transform.position.z
			);
	}

	void setCameraPosY(float y)
	{
		stageCamera.transform.position = new Vector3(
				stageCamera.transform.position.x, y, stageCamera.transform.position.z
			);
	}

	void setCameraPos(Vector2 pos)
	{
		stageCamera.transform.position = new Vector3(
			pos.x,
			pos.y,
			stageCamera.transform.position.z
			);
	}

	#endregion
}
