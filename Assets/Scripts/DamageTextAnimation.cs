﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamageTextAnimation : MonoBehaviour
{
	public float move_y_amount = 0.5f;
	TextMeshPro tm;

	//private void Start()
	//{
	//	initialize(Vector3.zero, 10);
	//}

	public void initialize(Vector3 init_pos, float damage)
	{
		transform.position = init_pos;

		tm = GetComponent<TextMeshPro>();
		tm.text = damage.ToString();
		iTween.MoveBy(gameObject,
			iTween.Hash("amount", new Vector3(0, move_y_amount, 0), "time", 1f,
				"easetype", iTween.EaseType.easeOutQuint));
			
			//new Vector3(0, move_y_amount, 0), 1f);
		iTween.ValueTo(gameObject,
				iTween.Hash("from", 1, "to", 0, "time", 1f, 
				"easetype", iTween.EaseType.easeOutQuint,
				"onupdate", "onTextColorUpdate",
				"onupdatetarget", gameObject, 
				"oncomplete", "onTextColorComplete", 
				"oncompletetarget",	gameObject
			));
	}

	void onTextColorUpdate(float val)
	{
		tm.color = new Color(tm.color.r, tm.color.g, tm.color.b, val);
	}

	void onTextColorComplete()
	{
		Destroy(gameObject);
	}
}
