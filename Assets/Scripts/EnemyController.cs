﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
	public int HP;

	private BoxCollider2D _hitbox;
	public BoxCollider2D hitbox {
		get
		{
			if(_hitbox == null)
			{
				_hitbox = GetComponent<BoxCollider2D>();
			}
			return _hitbox;
		}
	}
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void onHit()
	{
			
	}
}
